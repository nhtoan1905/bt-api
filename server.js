//import express from 'express';
 var express = require('express')

const app = express();

app.set('port', process.env.PORT || 9000);

app.get("/", (req,res) => {
    res.json("Hello everybody to my site :)");
});

app.listen(app.get('port'), ()=>{
    console.log(`Server running on port: ${app.get('port')}....`);
});